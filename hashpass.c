#include <stdio.h>
#include <stdlib.h>
#include "md5.h"
#include <errno.h>
#include <string.h>

int main(int argc, char *argv[]) {
    
    if (argc < 3) {
        perror("Enter source and destination");
        exit(1);
    }
    
    FILE *src = fopen(argv[1], "r");
    if (!src) {
        perror("Error in source file");
        exit(1);
    }
    
    FILE *dest = fopen(argv[2], "w");
    if (!dest) {
        perror("Error in destination file");
        exit(1);
    }
    
    char line[33];
    char* hashes;
    while (fgets(line, 33, src) !=NULL) {
        hashes =  md5(line, strlen(line)-1);
        fprintf(dest, "%s\n", hashes);
        free(hashes);
    }
}